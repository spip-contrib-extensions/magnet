<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/magnet?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_activer_magnet_objets' => 'تفعيل المغنايس على العناصر',
	'label_demagnetize' => 'إزالة',
	'label_down' => 'نزول',
	'label_magnetize' => 'مغنطة',
	'label_up' => 'صعود',

	// T
	'titre_page_configurer' => 'إعداد ملحق المغناطيس'
);
