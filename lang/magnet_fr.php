<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/magnet.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'label_activer_magnet_objets' => 'Activer les aimants sur les objets',
	'label_demagnetize' => 'Enlever',
	'label_down' => 'Descendre',
	'label_magnetize' => 'Aimanter',
	'label_up' => 'Monter',

	// M
	'magnet_list_articles' => 'Liste des articles aimantés',

	// P
	'page_info' => 'Cette page vous permet de gérer les articles aimantés',

	// T
	'titre_page_configurer' => 'Configuration de Magnet'
);
